Projeto de Bitcoin como um teste para a Hrestart!

A idéia é permitir que o usuário coloque um 'range' de datas válidas nos inputs para que a aplicação gere um gráfico que análisa por todo o range do usuário, o fechamento do bitcoin no dias em questão.

Usei a lib "ChartsJs" para gerar os gráficos.

Dei fetch em duas APis: Coin Paprika (para pegar as informações de data e fechamento diário da bitcoin) e a Currency Converter API (www.currencyconverterapi.com) para fazer a conversão do preço da bitcoin que estava em dólar, para real.


script.js:
Para conseguir levar os dados da API para o gráfico, criei 2 arrays vazios, o xlabels e ylabels (usei a função .push()) e usei atrelado a ele a função formataData() pra trazer um visual mais agradável nas datas do gráfico. Por fim, inseri os arrays com os dados na parte de gerar o gráfico.
Criei também uma função que limpa as informações do input apagando o gráfico, para trazer uma melhor experiência para o usuário.

validador.js:
Usei esse .js para fazer todas as validações corretamente, seguindo as regras recebidas no pdf da hrestart. Também usei um pouco de JQuery para setar o comportamento do input ao receber o dado que o usuário vai colocar.

style.css: 
Usei um pouco de css para deixar o visual mais agradável para proporcionar uma melhor experiência para o usuário.

index.html:
Toda a estruturação do meu site. Usei bootstrap para me ajudar na responsividade assim como para a estilização de alguns componentes.

Obrigado!